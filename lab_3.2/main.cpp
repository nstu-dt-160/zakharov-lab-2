﻿#include <iostream>
#include "DateTime.h"
#include "TimeInterval.h"

int main()
{
    try {
        DateTime d1 = DateTime(2023, 1, 1, 12, 15);
        DateTime d2 = DateTime(2021, 8, 25, 14, 30);

        std::cout << "Date 1: "; d1.print();
        std::cout << "Date 2: "; d2.print();
        std::cout << "-----------------------------------------------\n";

        TimeInterval i1 = TimeInterval(800, 2, 5);
        std::cout << "Date 1 + (800days 2h 5min): ";
        d1 += i1;
        d1.print();
        std::cout << "-----------------------------------------------\n";
        std::cout << "Date 1 - (800days 2h 5min): ";
        d1 -= i1;
        d1.print();
        std::cout << "-----------------------------------------------\n";

        std::cout << "Date 1 - Date2: ";
        i1 = d1 - d2;
        i1.print();
        std::cout << "-----------------------------------------------\n";

        std::cout << "Date 1 to char*: ";
        char* cstr = d1;
        std::cout << cstr;
        std::cout << "\n-----------------------------------------------";
    }
    catch (const std::string e) {
        std::cout << "[ERROR]: " << e << '\n';
        return -1;
    }

    return 0;
}