#pragma once
#include <string>
#include "TimeInterval.h"

extern bool G_DEBUG; // ����� ���������� ����������

class DateTime
{
	friend class TimeInterval;

public:
	DateTime();
	DateTime(int year, int month, int day, int hour, int minute);
	DateTime(const DateTime& dateTime);

	~DateTime();

	int getYear() { return year; }
	int getMonth() { return month; }
	int getDay() { return day; }
	int getHour() { return hour; }
	int getMinute() { return minute; }

	void setDate(int year, int month, int day);
	void setTime(int hour, int minute);

	void operator = (DateTime date);
	bool operator == (DateTime date);
	DateTime operator + (TimeInterval &interval);
	DateTime operator += (TimeInterval& interval);
	DateTime operator - (TimeInterval &interval);
	DateTime operator -= (TimeInterval& interval);

	operator char* ();

	TimeInterval operator - (DateTime& date);

	std::string toStr();
	static std::string dateTimeToStr(DateTime datetime);

	void print();

private:
	int year;
	int month;
	int day;
	int hour;
	int minute;
	std::string* dateTime_str;
	
	static int getDaysInMonth(int year, int month);
	static bool isLeapYear(int year);
	
	static void validateDate(int year, int month, int day);
	static void validateTime(int hour, int minute);

	DateTime addInterval(int day = 0, int hour = 0, int minute = 0);
	DateTime minimal(DateTime date1, DateTime date2);
};